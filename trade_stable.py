
import os
import re
import time
import random
from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import requests
from selenium.webdriver.chrome.service import Service
bal=-1
opi=1
def get_driver() -> webdriver:
    options = Options()
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                         "Chrome/104.0.0.0 Safari/537.36")
    options.add_argument("--start-maximized")

    # TODO: Add Headless Mode and Fix check_win in headless mode
    # Headless does work on typing and making trades but check_win failed on my test.
    # options.add_argument("--headless")

    options.add_argument(f"user-data-dir=/chrome profile")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_experimental_option('useAutomationExtension', False)
    options.add_argument("--disable-blink-features=AutomationControlled")
    options.add_argument("--disable-extensions")
    options.add_argument('--log-level=3')
    s = Service('chromedriver')
    return webdriver.Chrome(service=s, options=options)

def enter_amount(web_driver: webdriver, amount: str):
    amount_element = web_driver.find_element(By.XPATH, '//*[@id="amount-counter"]/div[1]/div[1]/vui-input-number/input')
    amount_element.send_keys(Keys.CONTROL + 'A', Keys.BACKSPACE)
    for character in amount:
        amount_element.send_keys(character)
        time.sleep(random.choice([0.2, 0.3, 0.5]))

def check_win(web_driver: webdriver, trade_amount: int) -> bool:
    while True:  # Keep looking for the Popup
        try:
            return_amount = web_driver.find_element(
                By.XPATH, '//*[@id="trade"]/div/div/app-toasts/app-option-toast/div/span[3]').text
            return_amount = float(re.findall(r'([-+]*\d+\.\d+|[-+]*\d+)', return_amount)[0])
            if return_amount > trade_amount:
                web_driver.find_element(  # Close the popup
                    By.XPATH, '//*[@id="trade"]/div/div/app-toasts/app-option-toast/div/button').click()
                return True
            elif return_amount <= trade_amount:
                web_driver.find_element(  # Close the popup
                    By.XPATH, '//*[@id="trade"]/div/div/app-toasts/app-option-toast/div/button').click()
                return False
        except NoSuchElementException:
            pass

def main():
    global bal
    global opi
    global balance
    # Configuration
    base_amount = float(70)
    martingale = float(2)
    martingale_stop = int(4)  # Reset the martingale counter and start from base amount again
    win_sleep = float(1)  # Will pause for n number of seconds after each win
    # Bot will Pause itself after account balance reaches or goes over the specified amount
    stop_balance = float(9999999)

    amount_index = 0
    amounts = []
    for i in range(martingale_stop):
        amounts.append(str(int(base_amount)))
        base_amount *= martingale

    driver = get_driver()
    driver.get("https://binomo.com/trading")
    while bal < 0:
        print("waiting")
        try:
            balance = driver.find_element(By.ID, 'qa_trading_balance').text.replace(',', '')
        except:
            print("An exception occurred")
        else:
            balance = driver.find_element(By.ID, 'qa_trading_balance').text.replace(',', '')
            print (bal)
            balance = float(re.findall(r'([-+]*\d+\.\d+|[-+]*\d+)', balance)[0])
            bal = balance
   # while True:
    #    balance = driver.find_element(By.ID, 'qa_trading_balance').text.replace(',', '')
     #   balance = float(re.findall(r'([-+]*\d+\.\d+|[-+]*\d+)', balance)[0])
      #  bal = balance
       # print(bal)
                


    
    
    
    
    call_opinion = driver.find_element(
    By.XPATH, '//*[@id="trade-menu"]/majority-opinion/div/div/div[2]/span[1]').text.replace('%', '').strip()
    opi = call_opinion
    response = requests.get('https://api.telegram.org/bot5234920052:AAEDxAVyUMHE5p_t1FwhC3HRUC9FycEz_Tc/sendMessage?chat_id=@algo_Tradeing&parse_mode=markdown&text='+'your initial balance is :'+str(bal))


main()
print (opi)
print (bal)
